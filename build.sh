#!/bin/bash
CMAKEARGS=""
if [[ -f "cmake/args" ]]; then
    CMAKEARGS="$(cat cmake/args)"
fi

./clean.sh

mkdir build
cd build || exit 1
cmake $CMAKEARGS ..
make "-j$(nproc)"
