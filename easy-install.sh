#!/bin/bash
CMAKEARGS=""
if [[ -f "cmake/args" ]]; then
    CMAKEARGS="$(cat cmake/args)"
fi

./clean.sh

mkdir build
cd build || exit 1
cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_BUILD_TYPE=Release $CMAKEARGS ..
make -j$(nproc) && sudo make install
