#include <atomic>
#include <csignal>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>
#include <kraken/globaltypes.hpp>
#include <kraken/utility/checksum.hpp>

std::atomic<bool> g_sigintRecv = false;

namespace fs = std::filesystem;

class FileOpenFailedError : public std::runtime_error
{
public:
    FileOpenFailedError(const std::string& arg)
        : std::runtime_error(arg)
    {}
};

class InterruptError : public std::runtime_error
{
public:
    InterruptError(const std::string& arg)
        : std::runtime_error(arg)
    {}
};

enum class Algorithm
{
    Sha256, Sha512,
};

enum class Command
{
    Resume, Update, Verify, CollectDatabases
};

struct Database
{
    std::map<fs::path, std::string> checksums;
    std::set<fs::path> unprocessed;
    std::set<fs::path> processed;
};

struct Options
{
    Algorithm algorithm = Algorithm::Sha512;
    Command command = Command::Resume;
    fs::path database = "checksums";
    fs::path path;
    bool deleteResumeManifest = false;
    bool showStatistics = true;
    bool verify = false;
    bool warnMissing = true;
    U64 maxFilesPerBlock = 1048576; // = 2^20
    U64 maxBytesPerBlock = 68719476736; // = 64 GiB
    U64 chunkSize = 524288; // = 0.5 MiB
};

struct Statistics
{
    std::vector<fs::path> failed;
    std::vector<fs::path> notFound;
    U64 untrackedFiles = 0;
    U64 bytesRead = 0;
    U64 filesRead = 0;
    U64 bytesVerified = 0;
    U64 filesVerified = 0;
};

extern "C" void sigintHandler(int)
{
    g_sigintRecv = true;
}

void showUsage(std::string thisName)
{
    std::cerr << "Usage: " << thisName <<
R"X*X( [command] [args].
Available arguments:
    --algorithm [algorithm] Which algorithm to use for checksums. Defaults to
                            sha512; available choices are "sha256" and "sha512".

    --database [name]       Where to locate/store the checksums. Defaults to
                            "checksums.algorithm", format is identical to that
                            of the sha256sum/sha512sum utilities and can be
                            verified using those.

    --delete-resume-info    Delete resume manifest if one exists and command
                            isn't resume.

    --no-delete-resume-info Do not delete resume manifest except when resume
                            command finishes (default).

    --no-show-statistics    Do not output statistics after completion.

    --no-verify             Do not verify stored checksums when updating.

    --no-warn-missing       Do not warn about missing files when updating
                            checksums.

    --path [path]           Path of directory containing the files. It should be
                            specified relative to the current working directory.

    --show-statistics       Output statistics after completion.

    --verify                Verify stored checksums when updating.

    --warn-missing          Warn if filenames from database cannot be found.
                            (default)

Available commands:
    collect   Collect all databases from subdirectories to form a single
              database (or two, if there are multiple checksum algorithms used).
    generate  Alias of update.

    resume    Resume a previously interrupted operation.

    update    Updates the checksums of the given path in the database. A warning
              will be generated for each missing file, unless --no-warn-missing
              is passed. If the database doesn't exist, a new one is created.

    verify    Verify the checksums of the database.
)X*X";
}

void writeOptions(std::ostream& output,
                  const Options& options)
{
    std::string algorithm;
    switch (options.algorithm)
    {
    case Algorithm::Sha256: algorithm = "sha256"; break;
    case Algorithm::Sha512: algorithm = "sha512"; break;
    }

    std::string command;
    switch (options.command)
    {
    case Command::CollectDatabases: command = "collect"; break;
    case Command::Resume: command = "resume"; break;
    case Command::Update: command = "update"; break;
    case Command::Verify: command = "verify"; break;
    }

    output << algorithm << " "
           << command << " "
           << options.database << " "
           << options.path << " "
           << options.showStatistics << " "
           << options.verify << " "
           << options.warnMissing << " "
           << options.maxFilesPerBlock << " "
           << options.maxBytesPerBlock << " "
           << options.chunkSize << "\n";
}

void readOptions(std::istream& input,
                 Options& options)
{
    std::string reader;
    input >> reader;
    if (reader == "sha256") options.algorithm = Algorithm::Sha256;
    else if (reader == "sha512") options.algorithm = Algorithm::Sha512;
    else throw std::runtime_error("Invalid algorithm.");
    input >> reader;
    if (reader == "resume") options.command = Command::Resume;
    else if (reader == "update") options.command = Command::Update;
    else if (reader == "verify") options.command = Command::Verify;
    else throw std::runtime_error("Invalid command.");

    input >> options.database
          >> options.path
          >> options.showStatistics
          >> options.verify
          >> options.warnMissing
          >> options.maxFilesPerBlock
          >> options.maxBytesPerBlock
          >> options.chunkSize;
    input.get();
}

bool parseOptions(const std::vector<std::string>& args,
                  Options& optionsOut)
{
    optionsOut = {};
    if (args.empty()) return false; // At least one command must be given.
    bool pathFound = false;
    bool databaseFound = false;
    for (size_t i = 0; i < args.size(); ++i)
    {
        if (args[i][0] != '-')
        {
            if (args[i] == "generate") optionsOut.command = Command::Update;
            else if (args[i] == "resume") optionsOut.command = Command::Resume;
            else if (args[i] == "update") optionsOut.command = Command::Update;
            else if (args[i] == "verify") optionsOut.command = Command::Verify;
            else if (args[i] == "collect") optionsOut.command = Command::CollectDatabases;
            else return false; // Invalid command.
        }
        else
        {
            if (args[i] == "--no-verify") optionsOut.verify = false;
            else if (args[i] == "--verify") optionsOut.verify = true;
            else if (args[i] == "--no-show-statistics") optionsOut.showStatistics = false;
            else if (args[i] == "--show-statistics") optionsOut.showStatistics = true;
            else if (args[i] == "--no-warn-missing") optionsOut.warnMissing = false;
            else if (args[i] == "--warn-missing") optionsOut.warnMissing = true;
            else if (args[i] == "--no-delete-resume-info") optionsOut.deleteResumeManifest = false;
            else if (args[i] == "--delete-resume-info") optionsOut.deleteResumeManifest = true;

            // All other options have an additional argument, so we check here
            // that they do indeed have such (and fail otherwise).
            else if (i+1 >= args.size()) return false;

            else if (args[i] == "--algorithm")
            {
                if (args[i+1] == "sha256") optionsOut.algorithm = Algorithm::Sha256;
                else if (args[i+1] == "sha512") optionsOut.algorithm = Algorithm::Sha512;
                else return false;
                ++i;
            }
            else if (args[i] == "--database")
            {
                optionsOut.database = args[i+1];
                databaseFound = true;
                ++i;
            }
            else if (args[i] == "--path")
            {
                optionsOut.path = args[i+1];
                pathFound = true;
                ++i;
            }

            else
            {
                return false;
            }
        }
    }

    if (!databaseFound)
    {
        optionsOut.database = (optionsOut.algorithm == Algorithm::Sha512)
                              ? "checksums.sha512"
                              : "checksums.sha256";
    }
    if (optionsOut.command == Command::CollectDatabases && !databaseFound)
    {
        optionsOut.database = "";
    }

    return pathFound
           || optionsOut.command == Command::Verify
           || optionsOut.command == Command::Resume;
}

std::string escapeFilename(std::string unescaped)
{
    std::string s;
    s.reserve(unescaped.size());
    for (size_t i = 0; i < unescaped.size(); ++i)
    {
        if (unescaped[i] == '\n')
        {
            s.push_back('\\');
            s.push_back('n');
        }
        else if (unescaped[i] == '\\')
        {
            s.push_back('\\');
            s.push_back('\\');
        }
        else
        {
            s.push_back(unescaped[i]);
        }
    }
    return s;
}

std::string unescapeFilename(std::string escaped)
{
    std::string s;
    s.reserve(escaped.size());
    for (size_t i = 0; i < escaped.size(); ++i)
    {
        if (escaped[i] == '\\')
        {
            ++i;
            if (i >= escaped.size()) break; // Bad escape.
            if (escaped[i] == 'n') s.push_back('\n');
            else if (escaped[i] == '\\') s.push_back('\\');
        }
        else
        {
            s.push_back(escaped[i]);
        }
    }
    return s;
}

template <typename ChecksumAlgorithm>
ChecksumAlgorithm computeFileChecksum(fs::path path, size_t chunkSize,
                                      size_t* readBytes)
{
    auto status = fs::status(path);
    if (status.type() == fs::file_type::directory)
    {
        throw FileOpenFailedError("Cannot open \"" + path.string()
                                  + "\" (is a directory).");
    }
    if (readBytes) *readBytes = 0;
    std::ifstream file(path, std::ios::binary);
    if (!file.is_open())
    {
        throw FileOpenFailedError("Failed to open \"" + path.string()
                                  + "\" for reading.");
    }

    ChecksumAlgorithm sum;

    auto bytedata = std::make_unique<U8[]>(chunkSize);
    size_t lastRead = chunkSize;
    while (lastRead == chunkSize)
    {
        if (g_sigintRecv)
        {
            throw InterruptError("Received SIGINT.");
        }
        file.read(reinterpret_cast<char*>(bytedata.get()), chunkSize);
        lastRead = file.gcount();
        sum.append(bytedata.get(), lastRead);
        if (readBytes) *readBytes += lastRead;
    }
    return sum;
}

fs::path getLockFilePath(const Options& options)
{
    auto lockPath = options.database;
    lockPath += ".lock";
    return lockPath;
}

fs::path getResumeManifestPath(const Options& options)
{
    auto resumePath = options.database;
    resumePath += ".resume";
    return resumePath;
}

bool setupLockFile(const Options& options)
{
    auto lockPath = getLockFilePath(options);
    if (fs::exists(lockPath)) return false;
    std::ofstream lockFile(lockPath);
    if (!lockFile.is_open()) return false;
    lockFile << "This is a temporary file which will be deleted once the "
                "appropriate program terminates.";
    return true;
}

void deleteLockFile(const Options& options)
{
    auto lockPath = getLockFilePath(options);
    if (!fs::exists(lockPath)) return; // Something is wrong, but the user could
                                       // just have deleted the file. In any
                                       // case, every other operation has
                                       // finished, so it is probably okay.
    fs::remove(lockPath);
}

class LockFileGuard
{
public:
    LockFileGuard(const Options& opts)
        : m_options{opts}, m_held{false} {}
    ~LockFileGuard()
    {
        if (m_held) deleteLockFile(m_options);
    }

    bool acquire() { return m_held = setupLockFile(m_options); }

private:
    const Options& m_options;
    bool m_held;
};

void setupSigintHandler()
{
    g_sigintRecv = false;
    std::signal(SIGINT, sigintHandler);
}

std::set<std::string> getIgnoredExtensions(fs::path databasePath)
{
    std::set<std::string> ignore;
    auto ignoreFilePath = databasePath;
    ignoreFilePath += ".ignore";
    if (!fs::exists(ignoreFilePath)) return ignore;

    std::ifstream input(ignoreFilePath, std::ios::binary);
    if (!input.is_open())
    {
        throw std::runtime_error("Failed to open ignore info "
                                 + escapeFilename(ignoreFilePath.string())
                                 + ".");
    }
    std::string ext;
    while (std::getline(input, ext).good())
    {
        if (!ext.empty())
        {
            ignore.insert(ext);
        }
    }
    return ignore;
}

// Recursively enumerates all files stored in directory; resulting file names
// are stored (unprocessed) in filesOut along with an empty string.
// It is assumed that the given path exists and is a directory (this is
// NOT CHECKED).
void enumerateFiles(const fs::path& relativeTo,
                    fs::path directory,
                    std::set<std::string> ignoreExt,
                    std::set<fs::path>& filesOut)
{
    try
    {
        for (const auto& path : fs::directory_iterator(directory))
        {
            switch (fs::symlink_status(path).type())
            {
            default:
            case fs::file_type::none:
            case fs::file_type::not_found:
            case fs::file_type::unknown:
                //throw std::logic_error("Failed to determine file type of " + path.string() + ".");
                break;

            case fs::file_type::regular:
                if (ignoreExt.find(fs::path(path).extension().string()) == ignoreExt.end())
                {
                    filesOut.insert(fs::relative(path, relativeTo));
                }
                break;

            case fs::file_type::directory:
                enumerateFiles(relativeTo, path, ignoreExt, filesOut);
                break;

            case fs::file_type::symlink:
            case fs::file_type::block:
            case fs::file_type::character:
            case fs::file_type::fifo:
            case fs::file_type::socket:
                break;
            }
        }
    }
    catch (fs::filesystem_error& err)
    {
        std::cerr << "Warning: " << err.what() << "\n";
    }
}

void readDatabase(std::ifstream& input,
                  std::map<fs::path, std::string>& dbOut)
{
    std::string reader;
    while (std::getline(input, reader).good())
    {
        std::string checksum;
        std::string escapedPath;
        size_t i = reader.at(0) == '\\';
        while (i < reader.size() && reader[i] != ' ')
        {
            checksum.push_back(reader[i++]);
        }
        if (i >= reader.size() || reader[i] != ' ')
        {
            throw std::runtime_error("Malformed line '" + reader + "' in database.");
        }
        ++i;
        if (i >= reader.size() || reader[i] != ' ')
        {
            throw std::runtime_error("Malformed line '" + reader + "' in database.");
        }
        ++i;
        while (i < reader.size())
        {
            escapedPath.push_back(reader[i++]);
        }

        if (checksum.size() != 64 && checksum.size() != 128)
        {
            throw std::runtime_error("Checksum does not correspond to known "
                                     "algorithm.");
        }

        dbOut[unescapeFilename(escapedPath)] = checksum;
    }
}

void readDatabase(fs::path path,
                  std::map<fs::path, std::string>& dbOut)
{
    std::ifstream input(path, std::ios::binary);
    if (!input.is_open())
    {
        throw FileOpenFailedError("Failed to open database for reading.");
    }
    return readDatabase(input, dbOut);
}

void writeDatabase(std::ostream& output,
                   const std::map<fs::path, std::string>& database)
{
    for (const auto& pair : database)
    {
        auto unescaped = pair.first.string();
        auto escaped = escapeFilename(unescaped);
        if (escaped.size() != unescaped.size()) output << '\\';
        output << pair.second << "  " << escaped << "\n";
    }
}

void writeDatabase(fs::path path,
                   std::map<fs::path, std::string>& dbOut)
{
    std::ofstream output(path, std::ios::binary | std::ios::trunc);
    if (!output.is_open())
    {
        throw FileOpenFailedError("Failed to open database for writing.");
    }
    return writeDatabase(output, dbOut);
}

void handleChecksumMismatch(fs::path path,
                            const Options&,
                            Statistics& statistics)
{
    statistics.failed.push_back(path);
    std::cerr << "WARNING: " << path << " does NOT match stored checksum!\n";
}

void handleMissingFile(fs::path path,
                       const Options& options,
                       Statistics& statistics)
{
    statistics.notFound.push_back(path);
    if (options.warnMissing)
    {
        std::cerr << "WARNING: " << path << " not found.\n";
    }
}

bool verifyChecksum(fs::path path, std::string sum,
                    const Options& options,
                    Statistics& statistics)
{
    if (!fs::exists(path))
    {
        handleMissingFile(path, options, statistics);
        return false;
    }
    U64 bytes = 0;
    std::string checksum;
    if (sum.size() == 64)
    {
        checksum = computeFileChecksum<kraken::SHA256sum>(path, options.chunkSize, &bytes).sum();
    }
    else if (sum.size() == 128)
    {
        checksum = computeFileChecksum<kraken::SHA512sum>(path, options.chunkSize, &bytes).sum();
    }
    else
    {
        throw std::runtime_error("Unknown checksum type.");
    }
    statistics.bytesVerified += bytes;
    if (sum != checksum)
    {
        handleChecksumMismatch(path, options, statistics);
        return false;
    }
    return true;
}

void runMainCommand(const Options& options,
                    Statistics& statistics,
                    Database& database)
{
    U64 currFiles = 0;
    U64 currBytes = 0;
    try
    {
        for (auto it = database.unprocessed.begin();
             it != database.unprocessed.end();
             it = database.unprocessed.erase(it))
        {
            if (currFiles >= options.maxFilesPerBlock
                || currBytes >= options.maxBytesPerBlock)
            {
                break;
            }

            auto knownIt = database.checksums.find(*it);
            auto procIt = database.processed.find(*it);


            bool knownSum = knownIt != database.checksums.end();
            bool processedBefore = procIt != database.processed.end();

            if (processedBefore) continue;

            if (knownSum)
            {
                if (options.verify || options.command == Command::Verify)
                {
                    U64 bytesBefore = statistics.bytesVerified;

                    if (verifyChecksum(knownIt->first,
                                       knownIt->second,
                                       options, statistics))
                    {
                        database.processed.insert(*it);
                    }

                    // Fix statistics so we can stop at the appropriate point.
                    currBytes += statistics.bytesVerified - bytesBefore;
                    ++statistics.filesVerified;
                    ++currFiles;
                }
            }
            else if (options.command == Command::Update)
            {
                U64 bytes = 0;
                std::string checksum;
                switch (options.algorithm)
                {
                case Algorithm::Sha256:
                    checksum = computeFileChecksum<kraken::SHA256sum>(*it, options.chunkSize, &bytes).sum();
                    break;
                case Algorithm::Sha512:
                    checksum = computeFileChecksum<kraken::SHA512sum>(*it, options.chunkSize, &bytes).sum();
                    break;
                }
                database.processed.insert(*it);
                database.checksums[*it] = checksum;

                statistics.bytesRead += bytes;
                ++statistics.filesRead;

                currBytes += bytes;
                ++currFiles;
            }
            else
            {
                ++statistics.untrackedFiles;
            }
        }
    }
    catch (const InterruptError&)
    {
        ;
    }
    catch (...)
    {
        throw;
    }
}

void readResumeManifest(Options& options,
                        Database& database)
{
    std::ifstream input(getResumeManifestPath(options), std::ios::binary);
    if (!input.is_open())
    {
        throw FileOpenFailedError("Failed to open resume manifest for reading.");
    }

    readOptions(input, options);

    size_t prevProcCount = 0;
    input >> prevProcCount;
    for (size_t i = 0; i < prevProcCount; ++i)
    {
        fs::path path;
        input >> path;
        database.processed.insert(path);
    }

    input.get();

    if (options.command == Command::Update)
    {
        readDatabase(input, database.checksums);
    }
}

void writeResumeManifest(const Options& options,
                         const Database& database)
{
    std::ofstream output(getResumeManifestPath(options), std::ios::binary | std::ios::trunc);
    if (!output.is_open())
    {
        throw FileOpenFailedError("Failed to open resume manifest for writing.");
    }

    writeOptions(output, options);

    output << database.processed.size() << "\n";
    for (const auto& path : database.processed)
    {
        output << path << "\n";
    }

    if (options.command == Command::Update)
    {
        // We only need to write computed checksums *if* we are updating.
        writeDatabase(output, database.checksums);
    }
}

void removeResumeManifest(const Options& options)
{
    fs::remove(getResumeManifestPath(options));
}

void run(const Options& options,
         Statistics& statistics,
         Database& database)
{
    while (!database.unprocessed.empty() && !g_sigintRecv)
    {
        runMainCommand(options, statistics, database);
        writeResumeManifest(options, database);
    }

    if (database.unprocessed.empty())
    {
        removeResumeManifest(options);
    }
}

std::string readableSize(U64 sz)
{
    if (sz < 1024) return std::to_string(sz) + "B";
    std::vector<std::string> prefixes = {"KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB"};
    U64 szCpy = sz;
    U64 oom = 0;
    while (szCpy >= 1024)
    {
        szCpy >>= 10;
        ++oom;
    }
    --oom;

    size_t p10digits = 100;

    double approx = static_cast<double>(sz) / static_cast<double>(1024ull << (10*oom)) + 0.5 / static_cast<double>(p10digits);
    size_t trunc = static_cast<size_t>(p10digits*approx);
    auto res = std::to_string(trunc / p10digits) + '.' + std::to_string(trunc % p10digits);
    while (res.back() == '0') res.pop_back();
    if (res.back() == '.') res.pop_back();
    return res + prefixes.at(oom);
}

void show(const Statistics& stats)
{
    std::cout << "Statistics:\n"
              << "Data read: " << readableSize(stats.bytesRead) << "\n"
              << "Data verified: " << readableSize(stats.bytesVerified) << "\n"
              << "Files read: " << stats.filesRead << "\n"
              << "Files verified: " << stats.filesVerified << "\n"
              << "Integrity failed: " << stats.failed.size() << "\n"
              << "Missing: " << stats.notFound.size() << "\n"
              << "Untracked files: " << stats.untrackedFiles << "\n"
    ;
}

void run(Options options)
{
    Database database;
    try
    {
        readDatabase(options.database, database.checksums);
    }
    catch (...)
    {
        if (options.command == Command::Verify) throw;
    }

    if (fs::exists(getResumeManifestPath(options)))
    {
        if (options.command != Command::Resume)
        {
            if (options.deleteResumeManifest)
            {
                // Remove resume manifest since the command isn't resume.
                fs::remove(getResumeManifestPath(options));
            }
            else
            {
                std::cerr << "Error: Resume manifest exists. Please resume "
                             "and finish the previous command, or delete the "
                             "resume manifest (or pass --delete-resume-info).\n";
                return;
            }
        }
        else
        {
            readResumeManifest(options, database);
        }
    }

    bool isValidPath = fs::exists(options.path) && fs::is_directory(options.path);
    if (!isValidPath && options.command == Command::Update)
    {
        throw std::runtime_error("Cannot update when given path is invalid.");
    }

    if (isValidPath)
    {
        auto ignoreExt = getIgnoredExtensions(options.database);
        enumerateFiles(options.path, options.path, ignoreExt, database.unprocessed);

        auto lockFilePath = fs::relative(getLockFilePath(options), options.path);
        auto it = database.unprocessed.find(lockFilePath);
        if (it != database.unprocessed.end())
        {
            database.unprocessed.erase(it);
        }
    }
    else
    {
        // If the path is not valid / not given, then the command is necessarily
        // verify, and thus we just need to process all files stored in
        // the checksums.
        for (const auto& entry : database.checksums)
        {
            database.unprocessed.insert(entry.first);
        }
    }

    /// Now, database contains the following data:
    /// checksums    All known checksums (either computed before or read from
    ///              resume manifest -- resume manifest takes precedence).
    /// unprocessed  Paths to files which have not yet been processed.
    /// processed    Paths to processed files (ignored from here on).

    Statistics statistics;

    while (!database.unprocessed.empty() && !g_sigintRecv)
    {
        runMainCommand(options, statistics, database);
        writeResumeManifest(options, database);
    }

    if (database.unprocessed.empty())
    {
        if (options.command == Command::Update)
        {
            writeDatabase(options.database, database.checksums);
        }

        if (database.unprocessed.empty())
        {
            removeResumeManifest(options);
        }
    }

    if (options.warnMissing && !statistics.notFound.empty())
    {
        std::cerr << "WARNING: The following files are missing:\n";
        for (auto& path : statistics.notFound)
        {
            std::cerr << escapeFilename(path.string()) << "\n";
        }
    }

    if (!statistics.failed.empty())
    {
        std::cerr << "WARNING: The following files failed the integrity check:\n";
        for (auto& path : statistics.failed)
        {
            std::cerr << escapeFilename(path.string()) << "\n";
        }
    }


    if (options.warnMissing && !statistics.notFound.empty()) std::cerr << "WARNING: There were missing files.\n";
    if (!statistics.failed.empty()) std::cerr << "WARNING: There were files failing the integrity check.\n";

    if (options.showStatistics) show(statistics);
}

void recursivelyCollectDatabases(const fs::path& origPath,
                                 fs::path directory,
                                 Database& sha256db,
                                 Database& sha512db)
{
    try
    {
        for (const auto& path : fs::directory_iterator(directory))
        {
            switch (fs::symlink_status(path).type())
            {
            case fs::file_type::regular:
                if (fs::path(path).extension().string() == ".sha256"
                    || fs::path(path).extension().string() == ".sha512")
                {
                    std::map<fs::path, std::string> tmpdb;
                    std::ifstream inputDB(fs::path(path), std::ios::binary);
                    if (!inputDB.is_open())
                    {
                        throw std::runtime_error("Failed to open " + fs::path(path).string() + " for reading.");
                    }
                    readDatabase(inputDB, tmpdb);

                    Database& appendTo = fs::path(path).extension().string() == ".sha256"
                                         ? sha256db : sha512db;

                    for (const auto& entry : tmpdb)
                    {
                        auto newpath = fs::relative(directory / entry.first, origPath);
                        appendTo.checksums[newpath] = entry.second;
                    }
                }
                break;

            case fs::file_type::directory:
                recursivelyCollectDatabases(origPath, path, sha256db, sha512db);
                break;

            default:
            case fs::file_type::none:
            case fs::file_type::not_found:
            case fs::file_type::unknown:
            case fs::file_type::symlink:
            case fs::file_type::block:
            case fs::file_type::character:
            case fs::file_type::fifo:
            case fs::file_type::socket:
                break;
            }
        }
    }
    catch (fs::filesystem_error& err)
    {
        std::cerr << "Warning: " << err.what() << "\n";
    }
}

int main(int argc, char* argv[])
{
    setupSigintHandler();

    std::vector<std::string> args;
    for (int i = 1; i < argc; ++i)
    {
        args.push_back(argv[i]);
    }

    Options options;
    if (!parseOptions(args, options))
    {
        std::cerr << "Error: Invalid arguments.\n";
        showUsage(fs::path(argv[0]).filename().string());
        return 1;
    }

    if (options.command == Command::CollectDatabases)
    {
        Database sha256db, sha512db;
        recursivelyCollectDatabases(options.path, options.path, sha256db, sha512db);
        if (options.database != "")
        {
            if (sha256db.checksums.empty()) writeDatabase(options.database, sha512db.checksums);
            else if (sha512db.checksums.empty()) writeDatabase(options.database, sha256db.checksums);
            else
            {
                auto path = options.database;
                path += ".sha256";
                writeDatabase(path, sha256db.checksums);
                path = options.database;
                path += ".sha512";
                writeDatabase(path, sha512db.checksums);
            }
        }
        else
        {
            writeDatabase(std::cout, sha256db.checksums);
            writeDatabase(std::cout, sha512db.checksums);
        }
    }
    else
    {
        LockFileGuard lg(options);
        if (!lg.acquire())
        {
            std::cerr << "Failed to set up lock file. Is another instance running?\n";
            return 1;
        }
        run(options);
    }
    return 0;
}
